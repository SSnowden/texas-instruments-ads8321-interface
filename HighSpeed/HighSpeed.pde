/**
 * Read 16-bit values over serial and display them as a simple graph
 * Each data point is split into 4 bytes. 
 * The high byte, the low byte, and two always 0 error check bytes
 * If an error is detected because the error bytes are not 0 then that reading is ignored.
 * The 16-bit values are scaled to fit on the screen
 */

import processing.serial.*;

Serial myPort;  // Create object from Serial class
int val, last, check;      // Data received from the serial port
int x;                     // The x position on the graph
void setup() 
{
  size(1680, 768);      // Set to your preferred window resolution
  // I know that the first port in the serial list on my mac
  // is always my  FTDI adaptor, so I open Serial.list()[0].
  // On Windows machines, this generally opens COM1.
  // Open whatever port is the one you're using.
  String portName = Serial.list()[1];
  println(Serial.list());
  myPort = new Serial(this, portName, 115200);
  
}

void keyPressed() {
  println(keyCode);

  if (key == 'c') {    // Begin requesting a sample and transmit of the data
    myPort.write('c');
    background(0);
    x=0;
  }
}

void draw()
{

  while ( myPort.available () > 2) {  // If data is available,
    stroke(255);     // Set normal points to white
    if (x%2 ==0) {

      val = myPort.read()<< 8;         // Read high byte first
    }
    else {
      val |= myPort.read();            // Then read low byte

      check = myPort.read() | myPort.read();    // Then read error bytes

      if(check != 0){                  // If the error check bytes are not 0 then shift things along
        stroke(255,0,0);
        println("Check failed");
       val = val << 8;
       val = check >> 8;
       check = check << 8;
       check |= myPort.read();
      }

      val = (val + 65536/2)%65535;    // Scale the sample to fit the screen

      point(x/2, val/100);            // Draw the sample

    }

    x++;
  }
}

