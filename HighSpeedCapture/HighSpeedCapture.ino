#define SPICLOCK PB_3     // Clock 
#define DATAIN PC_4       // MISO 
#define SELPIN PC_5       // Selection Pin
#define BUFF_SIZE 6000    // Will be able to take BUFF_SIZE / 4 samples

/************************************************************
Code for interfacing with a Texas Instruments ADS8321 16-Bit, high speed ADC
This program reads the ADC value over using its proprietry SPI/SSI serial interface
It stores the samples in an array until the array is full, and then transmits the data over serial
The values are 16 bit, and two stop bytes of 0x00 are used for error checking

*************************************************************/
int16_t readvalue; 

byte buff[BUFF_SIZE];

unsigned long sample;
byte record;
int divider = 4;

void setup(){ 
 //set pin modes 
 pinMode(SELPIN, OUTPUT); 
 pinMode(DATAIN, INPUT); 
 pinMode(SPICLOCK, OUTPUT); 
 //disable device to start with 
 digitalWrite(SELPIN,HIGH); 
 digitalWrite(SPICLOCK,LOW); 

 Serial.begin(115200);  // Go fast because why not
 record = 0;            // Record mode flag
 
 // Initialise the array to 1s (0 is not allowed)
 for(int i = 0; i<BUFF_SIZE;i++){
     buff[i] = 1; 
  }
} 

// Read the 16 bit ADC value over SPI
int read_adc(){
  int16_t adcvalue = 0;

  digitalWrite(SELPIN,LOW);            //Select ADC
  
  for(int i = 0; i<6; i++){
    digitalWrite(SPICLOCK,HIGH);       // 6 clock cycles for sampling
    digitalWrite(SPICLOCK,LOW);
  }

  //read bits from adc
  for (int i=15; i>=0; i--){
    //cycle clock
    digitalWrite(SPICLOCK,HIGH);
    adcvalue|=digitalRead(DATAIN)<<i;  // Read data bit
    digitalWrite(SPICLOCK,LOW);
  }

  digitalWrite(SELPIN, HIGH);          //turn off device
  return adcvalue;
}

void loop() { 
  // If recording mode
  while(record == 1){
    
    // If there's space in the buffer
    if(sample < BUFF_SIZE){
      // Read the ADC
      readvalue = read_adc();
      if(readvalue == 0){
       readvalue++;      // 0 is used for stop bits so make 0 vals be 1 (uncommon in a 16 bit ADC reading)
      }
      
      buff[sample] = readvalue >> 8;      // Store the high byte first
      buff[1 + sample] = readvalue;       // Store the low byte
      
      buff[2 + sample] = 0;               // Check bits
      buff[3 + sample] = 0;               // Check bits
      sample +=4;
    }
    else{
      // No more space in buffer, so reset and transmit data to client
      record = 0;
      sample = 0;
      
      Serial.write(buff,BUFF_SIZE);   // Transmit data
      for(int i = 0; i<BUFF_SIZE;i+=2){
         buff[i] = 1; buff[i+1] = 1; 
      }
    }
  }
 delay(250); 
} 

// When we get some serial data
void serialEvent(){
  if(Serial.available() > 0){
    char cha = Serial.read();
    
    if(cha == 'c'){
       sample = 0;
       record = 1;
    }

  }
  while(Serial.available() > 0){
   Serial.read(); 
  }
}
